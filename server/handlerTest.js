const { Task } = require('./dbModel')

const rootHandler = async (req,h) => {
      return h.response('this is root').code(200)
}
const getTasks = async (req,h) => {
      const query=req.query
      const isEmpty=JSON.stringify(query)=='{}'
      if(isEmpty){
            const result = await Task.find().lean()
            return h.response(result).code(200)
      }else{
            let sortBy=query.sort?query.sort:'status'
            let filter=query.filter?query.filter:{status:'new',status:'on-progress'}
            const result = await Task.find(filter,{_id:0,__v:0}).sort({[sortBy]:1}).lean()
            return h.response(result).code(200)
      }
}
const getTasksID = async (req,h) => {
      try {
            const result = await Task.findOne({id:req.params.id}).lean()
            return h.response(result).code(200)
        } catch (err) {
            return h.response(err).code(500)
        }
}
const getTasksIncomplete = async (req,h) => {
      try{
            const result=await Task.find({status:{$in:["new","on-progress"]}},{_id:0,__v:0}).lean()
            return h.response(result).code(200) 
      }catch{
            return h.response(notFoundResponse).code(404)
      }
}
const postTasks = async (req,h) => {
      try{
            const tasks=await Task.aggregate([
                  {
                      $group:{
                          _id:'max',
                          maxId:{
                              $max:'$id',
                          }
                      }
                  }
            ]) 
            let idnext=tasks[0].maxId+1
            let stats= req.payload.status = 'new'
            let cmts= req.payload.comments = []
            let newTask=Object.assign(req.payload,{id:idnext,status:stats,comments:cmts})
            const result = await Task.insertMany([newTask])
            return h.response(result).code(201)
      }catch(err){
            return h.response(err).code(400)
      }
}
const putTasks = async (req,h) => {
      try{
            const result=await Task.findOneAndUpdate({id:req.params.id},req.payload).lean()
            return h.response(result).code(200)
      }catch(err){
            return h.response(err).code(500)
      }

}
const delTasks = async (req,h) => {
      try{
            const result=await Task.deleteOne({id:req.params.id})
            return h.response(result).code(202)
      } catch (error) {
            return h.response(error).code(500)
        }
}

module.exports = { rootHandler,getTasks,getTasksID,getTasksIncomplete,postTasks,putTasks,delTasks }