const Joi = require('@hapi/joi')
const { rootHandler,getUsers,postUsers } = require('./handler')

const routes = [
      {
            method: 'GET',
            path: '/',
            handler: rootHandler
      },{
            method: 'GET',
            path: '/users',
            handler: getUsers,            
      },{
            method: 'POST',
            path: '/users',
            handler: postUsers,
            // options: {
            //       validate: {
            //           payload: {
            //               title: Joi.string().required(),
            //               desc: Joi.string().required(),
            //               deadline: Joi.date().required()
            //           }
            //       }
            // }      
      }
]

module.exports = {routes}