const Joi = require('@hapi/joi')
const { rootHandler,getTasks, getTasksID,getTasksIncomplete,postTasks,putTasks,delTasks } = require('./handler')

const routes = [
      {
            method: 'GET',
            path: '/',
            handler: rootHandler
      },{
            method: 'GET',
            path: '/tasks',
            handler: getTasks,            
      },{
            method: 'GET',
            path: '/tasks/{id}',
            handler: getTasksID,
            options: {
                  validate: {
                      params: {
                          id: Joi.number().positive().integer()
                      }
                  }
            }                  
      },{
            method: 'GET',
            path: '/tasks/incomplete',
            handler: getTasksIncomplete            
      },{
            method: 'POST',
            path: '/tasks',
            handler: postTasks,
            options: {
                  validate: {
                      payload: {
                          title: Joi.string().required(),
                          desc: Joi.string().required(),
                          deadline: Joi.date().required()
                      }
                  }
            }      
      },{
            method: 'PUT',
            path: '/tasks/{id}',
            handler: putTasks,
            options: {
                  validate: {
                      params: {
                          id: Joi.number().integer().required()
                      },
                      payload: {
                          title: Joi.string(),
                          desc: Joi.string(),
                          deadline: Joi.date(),
                          comments: Joi.string(),
                          status: Joi.string().valid('on-progress', 'complete')
                      }
                  }
            }                  
      },{
            method: 'DELETE',
            path: '/tasks/{id}',
            handler: delTasks,
            options: {
                  validate: {
                      params: {
                          id: Joi.number().integer().required()
                      }
                  }
            }                  
      }
]

module.exports = {routes}