const mongoose = require('mongoose')
const dbConnect = async () => {
      try{
            await mongoose.connect('mongodb://localhost:27017/photoSharing_test',{
                  useNewUrlParser: true,
                  useCreateIndex: true,
                  useUnifiedTopology: true,
                  useFindAndModify: false
            })
            mongoose.set('debug',true)
            console.log('MongoDB connect')
      }catch (err){
            console.log(err.message)
            process.exit(1)
      }
}
module.exports = { dbConnect }