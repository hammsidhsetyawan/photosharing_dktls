const Hapi = require('@hapi/hapi')
const r = require('./route')
const Qs = require('qs')
const { dbConnect } = require('./dbConnect')

const start = async() => {
      const server = Hapi.server({
            port: 8000,
            host: '0.0.0.0',
            query:{
                  parser: (query) => Qs.parse(query)
            }
      })
      dbConnect()
      server.route(r.routes)
      await server.start()
      console.log('Server running at:', server.info.uri);
      return server
}
start()