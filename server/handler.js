

const { User } = require('./dbModel')

const rootHandler = async (req,h) => {
      return h.response('this is root').code(200)
}

const getUsers = async (req,h) =>{
      const query=req.query
      const isEmpty=JSON.stringify(query)=='{}'
      if(isEmpty){
            const result = await User.find().lean()
            return h.response(result).code(200)
      }else{
            let sortBy=query.sort?query.sort:'status'
            let filter=query.filter?query.filter:{status:'new',status:'on-progress'}
            const result = await User.find(filter,{_id:0,__v:0}).sort({[sortBy]:1}).lean()
            return h.response(result).code(200)
      }
}

const postUsers = async (req,h) => {
      try{
            const users=await User.aggregate([
                  {
                      $group:{
                          _id:'max',
                          maxId:{
                              $max:'$id',
                          }
                      }
                  }
            ]) 
            let idnext=users[0].maxId+1
            // let stats= req.payload.status = 'new'
            let crat= req.payload.createdAt = new Date()
            let newUser=Object.assign(req.payload,{id:idnext,createdAt:crat})
            const result = await User.insertMany([newUser])
            return h.response(result).code(201)
      }catch(err){
            return h.response(err).code(400)
      }
}

module.exports = { rootHandler,getUsers,postUsers}