const mongoose = require('mongoose')
const { Schema } = mongoose

const userModel = new Schema({
      id: Number,
      userId: String,
      emailId: String,
      password: String,
      name: String,
      // createdAt: {type: Date, default: Date.now},
      createdAt: Date,
      updatedAt: Date,
      avatar: String
})
const User = mongoose.model('users', userModel)
  
module.exports = { User }