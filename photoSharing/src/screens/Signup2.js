
import React, {Component} from 'react';
import {TextInput,Text, View, TouchableOpacity,Button} from 'react-native';
import styles from '../styles/appStyle';
import axios from 'axios';

import { useDispatch } from 'react-redux'
import { connect } from "react-redux";
import { register } from '../redux/actions/auth';

export default class Signup extends Component {
  static navigationOptions = {
    title: 'Signup',
  };
  constructor(props) {
    super(props);
    this.state = {
      user:{
        emailId: '',
        password: '',
        rpassword: ''
      }
    };
  }

  async handleSignup(){
    const data = this.state.user;
    const dispatch = useDispatch();
    await dispatch(register(data))
    .then(response => {
      if(response.value.data.status === 200){
        setTimeout(() =>{
          this.props.navigation.navigate('Login')
        }, 500);
      }

    })
    .catch(error => alert(error.value.data.message))

    try{
      let postSignup = await axios.post('http://192.168.0.114:8000/users', {
        emailId: this.state.emailId,
        password: this.state.password,
      })
      if(postSignup){
        alert('Welcome')
        this.props.navigation.navigate('PreSignup')
      }

    }catch(error){
      alert(error)
    }
  }

  handleButton(){
    const {emailId,password,rpassword} = this.state.user
    const bOn = <TouchableOpacity activeOpacity={.7} style={styles.btnLogin} onPress={()=> this.handleSignup()} disabled={false}><Text style={styles.textsubmit} > SIGNUP </Text></TouchableOpacity>
    const bOff = <TouchableOpacity activeOpacity={.7} style={styles.btnLogin} onPress={()=> this.handleSignup()} disabled={true}><Text style={styles.textsubmit} > Not Correct </Text></TouchableOpacity>
    if(password !== rpassword || password === "" || emailId === "" || !emailId.match(/@RealUniversity.com\b/gi)){
      return bOff
    }
    return bOn
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <>
        <View style={{flex: 1, marginHorizontal: 35,justifyContent:'center'}}>
          <View style={styles.ViewText}>
            <Text style={styles.Text}>Email </Text>
          </View>
          <View>
            <TextInput
              onChangeText={text =>
                this.setState({
                  user: {...this.state.user, emailId: text},
                })
              }
              style={styles.TextInput}
              placeholder={'Your Email'}
            />
          </View>

          <View style={styles.ViewText}>
            <Text style={styles.Text}>Password </Text>
          </View>
          <View>
            <TextInput
              type='password'
              onChangeText={text =>
                this.setState({
                  user: {...this.state.user, password: text},
                })
              }
              style={styles.TextInput}
              placeholder={'Your Password'}
            />
          </View>

          <View style={styles.ViewText}>
            <Text style={styles.Text}>Re-type Password </Text>
          </View>
          <View>
            <TextInput
              type='password'
              onChangeText={text =>
                this.setState({
                  user: {...this.state.user, rpassword: text},
                })
              }
              style={styles.TextInput}
              placeholder={'Retype your Password'}
            />
          </View>
        {this.handleButton()}

        <View style={{marginTop: 15, flexDirection:'row'}}>
          <Text style={{ fontSize: 15}}>Have Account?</Text>
          <TouchableOpacity onPress = {() =>navigate('Login')}>
            <Text style={{ fontSize: 15, fontWeight:'bold', color:'#2bb358'}}> Login Now </Text>
          </TouchableOpacity>
        </View>



        </View>
      </>
    );
  }
}
