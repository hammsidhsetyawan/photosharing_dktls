
import React, {Component} from 'react';
import {TextInput,Text, View, TouchableOpacity,Dimensions} from 'react-native';
import styles from '../styles/appStyle';
import axios from 'axios';


export default class Login extends Component {
  static navigationOptions = {
    title: 'Login',
  };
  constructor(props) {
    super(props);
    this.state = {
      user: [],
    };
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <>
        <View style={{flex: 1, marginHorizontal: 35, justifyContent: 'center'}}>
          <View style={styles.ViewText}>
            <Text style={styles.Text}>Email </Text>
          </View>
          <View>
            <TextInput
              onChangeText={text =>
                this.setState({
                  task: {...this.state.task, title: text},
                })
              }
              style={styles.TextInput}
              placeholder={'Your username'}
            />
          </View>

          <View style={styles.ViewText}>
            <Text style={styles.Text}>Password </Text>
          </View>
          <View>
            <TextInput
              multiline={true}
              numberOfLines={12}
              textAlignVertical="top"
              onChangeText={text =>
                this.setState({
                  task: {...this.state.task, desc: text},
                })
              }
              style={styles.TextInput}
              placeholder={'Your Password'}
            />
          </View>

        <TouchableOpacity activeOpacity={.7} style={styles.btnLogin} onPress={()=> handleLogin()}>
          <Text style={styles.textsubmit} > LOGIN </Text>
        </TouchableOpacity>

        <View style={{marginTop: 15, flexDirection:'row'}}>
          <Text style={{ fontSize: 15}}>Dont Have Account?</Text>
          <TouchableOpacity onPress = {() =>navigate('Signup')}>
            <Text style={{ fontSize: 15, fontWeight:'bold', color:'#2bb358'}}> Register Now </Text>
          </TouchableOpacity>
        </View>



        </View>
      </>
    );
  }

//   componentWillUnmount() {
//     axios.post('http://192.168.0.114:8000/tasks', this.state.task, {});
//   }
}
