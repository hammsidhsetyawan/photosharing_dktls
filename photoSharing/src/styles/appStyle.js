import {StyleSheet} from 'react-native';

const appStyles = StyleSheet.create({
  container: {
    margin: 20,
    flex: 1,
  },
  txtHeader: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: '#fff',
  },
  ViewText: {
    alignItems: 'center',
    paddingBottom: 5,
    flexDirection: 'row',
    paddingTop: 15,
  },
  ViewTextInput: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: 6,
  },
  Text: {
    fontSize: 19,
    paddingLeft: 3,
    fontWeight: 'bold',
    color: '#787878',
  },
  TextInput: {
    height: 40,
    paddingLeft: 8,
    borderColor: '#787878',
    borderWidth: 0.5,
    borderRadius: 7,
  },
  btnLogin: {
    height: 45,
    borderRadius: 45,
    backgroundColor: '#2bb358',
    justifyContent: 'center',
    marginTop: 30,
  },
  textsubmit: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold'
  },
});

export default appStyles
