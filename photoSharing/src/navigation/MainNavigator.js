import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import Login from '../screens/Login';
import Signup from '../screens/Signup';
const StackAuth = createStackNavigator(
    {
      Login,
      Signup,
    },
    {
      initialRouteName: 'Login',
      headerMode: 'none',
    }
);


// const StackHome = createStackNavigator(
//     {

//     },
//     {
//       initialRouteName: 'Feed',
//       headerMode: 'none',
//     }
// );

const Router = createSwitchNavigator(
    {
      StackAuth,
      // StackHome,
    },
    {
      initialRouteName: 'StackAuth',
      headerMode: 'none',
    }
);



export default createAppContainer(Router);
