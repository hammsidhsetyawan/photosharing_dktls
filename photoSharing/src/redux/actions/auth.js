import axios from 'axios';
import AsyncStorage from 'react-native';

export const register = (input) => {
      console.log(input);
      return {
        type: 'REGISTER',
        payload: axios.post('http://192.168.0.114:8000/users',input)
      };
    };
    
    export const login = (input) => {
      return {
        type: 'LOGIN',
        payload: axios.post('http://192.168.0.114:8000/users',input)
      };
    };
    
    export const getItem = () => {
      return {
        type: 'ITEM',
        payload: AsyncStorage.getItem('user')
      }
    }