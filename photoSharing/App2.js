import React from 'react';
import {StatusBar} from 'react-native';
import MainNavigator from './src/navigation/MainNavigator';
import {Provider} from 'react-redux';
import store from './src/redux/store';

const App = () => {
      return (
          <>
          <StatusBar backgroundColor='#0DAC50' />
              <Provider store={store}>
                <MainNavigator />
              </Provider>
          </>
      );
};
    
export default App;