import React, { useEffect } from 'react';
import MainNavigator from './src/navigation/MainNavigator';

export default class App extends React.Component {
  render() {
    return <MainNavigator />;
  }
}